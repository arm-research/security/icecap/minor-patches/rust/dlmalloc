use core::ptr;

extern "C" {
    static mut icecap_runtime_heap_start: usize;
    static icecap_runtime_heap_end: usize;
}

pub unsafe fn alloc(size: usize) -> (*mut u8, usize, u32) {
    let addr = icecap_runtime_heap_start;
    icecap_runtime_heap_start += size;
    if icecap_runtime_heap_start > icecap_runtime_heap_end {
        (ptr::null_mut(), 0, 0)
    } else {
        (addr as *mut u8, size, 0)
    }
}

pub unsafe fn remap(_ptr: *mut u8, _oldsize: usize, _newsize: usize, _can_move: bool) -> *mut u8 {
    ptr::null_mut()
}

pub unsafe fn free_part(_ptr: *mut u8, _oldsize: usize, _newsize: usize) -> bool {
    false
}

pub unsafe fn free(_ptr: *mut u8, _size: usize) -> bool {
    false
}

pub fn can_release_part(_flags: u32) -> bool {
    false
}

pub fn allocates_zeros() -> bool {
    true
}

pub fn page_size() -> usize {
    4096
}
